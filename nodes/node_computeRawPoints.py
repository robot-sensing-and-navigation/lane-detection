#!/usr/bin/env python3
# Subscribes to image and computes raw points

import rospy
import cv2
import sys
import numpy as np
import time
from cv_bridge import CvBridge, CvBridgeError

from scripts.lib import settings
from scripts.lib import pipeline_lanefinding as lane_finding
from scripts.lib import pipeline_preprocessing as pre_processing
from scripts.lib.lib_photography import *
from scripts.lib import lib_perspective as perspective
from scripts.lib import lib_linedetection as linedetection
from scripts.lib.lib_frame import *

from sensor_msgs.msg import Image
from geometry_msgs.msg import PoseArray
from copy import deepcopy


class ComputeRawPoints:

    def __init__(self):
        rospy.init_node('ComputeRawPoint', anonymous=True)
        self.pub = rospy.Publisher('RawPoints', PoseArray, queue_size=100)
        self.pubRec = PoseArray()  # Create publisher message object once
        self.cvBridge = CvBridge()
        self.preprocessing_obj1 = pre_processing.pipeline_preprocessing(
            settings.param_1)
        self.preprocessing_obj2 = pre_processing.pipeline_preprocessing(
            settings.param_2)
        self.preprocessing_obj = pre_processing.pipeline_preprocessing(
            settings.param_2)
        self.perspective_obj = perspective.birdseye_perspective(4)
        self.perspective_obj1 = perspective.birdseye_perspective(4)
        self.perspective_obj2 = perspective.birdseye_perspective(4)

        self.lanedetection_obj = lane_finding.pipeline_lanefinding()
        print("INIT")
        rospy.Subscriber('CameraData', Image, self.callback)
        if cv2.waitKey(10) & 0xFF == ord('q'):
            cv2.destroyAllWindows()
        rospy.spin()

    def apply_laneDetection(self, frame):
        start_time = time.time()
        # Apply sobel processing on normal view and get the birds eye view from it.
        self.preprocessing_obj1.apply_pipeline(frame)
        self.perspective_obj1.apply_skyview(
            frameHLS2RGB(self.preprocessing_obj1.frame_HLS))
        # cv2.imshow('framehls sky', perspective_obj.frame_skyView)

        # Apply Sobel processing on BEV directly
        self.perspective_obj.apply_skyview(frame)
        self.preprocessing_obj.apply_pipeline(
            self.perspective_obj.frame_skyView)

        # Fuse detected edge masks from image-> preprocessing + Sobel -> BEV  and image -> BEV -> preprocessing + Sobel
        fused = deepcopy(self.perspective_obj1.frame_skyView) | deepcopy(frameHLS2RGB(
            self.preprocessing_obj.frame_HLS))

        # kernel = np.ones((5, 5), np.uint8)
        # fused = cv2.dilate(fused, kernel, iterations=1)

        # Image Sharpening
        kernel = np.array([[0, -1, 0],
                           [-1, 5, -1],
                           [0, -1, 0]])
        fused = cv2.filter2D(src=fused, ddepth=-1, kernel=kernel)

        # Second time preprocessing + ED lines on fused BEV
        self.preprocessing_obj2.apply_pipeline_noSobel(
            fused, apply_mask=True)

        # ED lines directly on image
        linedrawing_obj = linedetection.line_detection(deepcopy(cv2.cvtColor(
            frame, cv2.COLOR_RGB2GRAY)))  # only using 1 time processing rn

        linedrawing_obj.ED_Lines()
        mask = np.zeros((frame.shape[0], frame.shape[1]), dtype=np.uint8)
        frame_copy = deepcopy(frame)

        for l in linedrawing_obj.lines:
            line_start, line_end = tuple(l[:2]), tuple(l[2:4])

            m = abs(np.rad2deg(np.arctan2(
                (line_end[1] - line_start[1]), (line_end[0] - line_start[0]))))
            if m > 10 and m <= 170:
                cv2.line(mask, line_start, line_end, (255, 255, 255), 1)

        for i in range(mask.shape[0] - 1):
            for j in range(mask.shape[1] - 1):
                if mask[i][j] == 0:
                    frame_copy[i][j] = [0, 0, 0]

        # Convert ED line detected frame to BEV
        self.perspective_obj2.apply_skyview(frame_copy)

        # Fuse (fused image -> 2nd preprocess + ED lines) and (image -> ED lines -> BEV)
        final_fused = deepcopy(self.perspective_obj2.frame_skyView) | deepcopy(frameHLS2RGB(
            self.preprocessing_obj2.frame_HLS))

        self.preprocessing_obj2.frame_HLS = final_fused

        self.lanedetection_obj.apply_pipeline(
            frame, self.preprocessing_obj1, self.preprocessing_obj2, self.perspective_obj1, self.perspective_obj2)

        # cv2.imshow("LinesDrawn", lanedetection_obj.dst_frame)
        # out.write(lanedetection_obj.dst_frame)
        # count = count+1
        # print(count, "out of", frame_count, "frames")
        elapsed_time = time.time() - start_time  # end timing
        print('Frame Preprocess Complete: Time: {0:.3f}s'.format(
            elapsed_time))

    def callback(self, data):
        """'GetImage' Subscriber callback functionsFor every image received, execute the image processing pipeline and publish necessary data for the overlay"""

        frame = self.cvBridge.imgmsg_to_cv2(
            data, desired_encoding="passthrough")

        frame = rescale_frame(frame, 66.67)
        self.apply_laneDetection(frame)
        # frame = rescale_frame(frame, 66.67)
        # self.preprocessing_obj1.apply_pipeline(frame)
        # self.perspective_obj.apply_skyview(
        #     frameHLS2RGB(self.preprocessing_obj1.frame_HLS))
        # self.preprocessing_obj2.apply_pipeline(
        #     self.perspective_obj.frame_skyView)
        # self.lanedetection_obj.apply_pipeline(
        #     frame, self.preprocessing_obj1, self.preprocessing_obj2)

        cv2.imshow("LinesDrawn", self.lanedetection_obj.dst_frame)
        if cv2.waitKey(10) & 0xFF == ord('q'):
            cv2.destroyAllWindows()

if __name__ == '__main__':
    try:
        comp = ComputeRawPoints()
    except rospy.ROSInterruptException:
        pass
