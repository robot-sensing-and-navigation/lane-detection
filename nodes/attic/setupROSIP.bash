IPADDR=`ifconfig | grep inet | head -1 | cut -f 10 -d ' '`
echo Setting ROS_IP to $IPADDR
export ROS_IP=$IPADDR
export ROS_MASTER_URI=http://$IPADDR:11311