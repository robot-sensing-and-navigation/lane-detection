#!/usr/bin/env python3
# Subscribes to image and computes raw points

import rospy
import cv2
import sys
import numpy as np
from sensor_msgs.msg import Image
from geometry_msgs.msg import PoseArray
from cv_bridge import CvBridge, CvBridgeError 
import time

class ComputeRawPoints:
    pub

    def __init__(self):
        rospy.init_node('ComputeRawPoint', anonymous=True)
        rospy.Subscriber('GetImage', Image, self.callback)
        pub = rospy.Publisher('RawPoints', PoseArray, queue_size=100)
        rospy.spin()


    def callback(self, data):
        pubRec = PoseArray()
        pubRec.Header = data.Header
        pubRec.poses[0].point.x = data.data[0]
        pubRec.poses[0].point.y = data.data[1]
        pub.publish(pubRec)
 
if __name__ == '__main__':
    try:
        comp = ComputeRawPoints()
    except rospy.ROSInterruptException:
        pass

