#!/usr/bin/env python
# Publishes video images

import rospy
import cv2
import sys
import numpy as np
from sensor_msgs.msg import Image
from std_msgs.msg import Bool
from cv_bridge import CvBridge, CvBridgeError 
import time


def readAndPublishVideo(verb = 0, imgFile = '', rosRate = 0.5, waitTime = 1000):
    if verb > 0:
        print('CameraData from {0} at {1} with wait {2}'.format(imgFile, rosRate, waitTime))

    pub = rospy.Publisher('CameraData', Image, queue_size=100)
    rospy.init_node('CameraDataPublisher', anonymous=True)
    rate = rospy.Rate(rosRate)
    bridge = CvBridge() #for conversion

    # TODO: Added this only to test | Remove
    pub2 =rospy.Publisher('LaneChangeStatus', Bool, queue_size=100)

    if verb > 1:
        print ("Trying to open resource:{0}".format(imgFile))

    cap = cv2.VideoCapture(imgFile)
    if not cap.isOpened():
        print ("Error opening: {0}".format(imgFile))
        exit(0)


    rval, frame = cap.read()
    while rval and not rospy.is_shutdown():
        cv2.imshow("Stream: " + imgFile, frame)
        rval, frame = cap.read()

        # ROS image stuff
        if frame is not None:
            frame = np.uint8(frame)
        camMessage = bridge.cv2_to_imgmsg(frame, encoding="passthrough")

        pub.publish(camMessage)
        # TODO: Added this only to test | Remove
        lanestatusMessage = Bool()
        lanestatusMessage.data = False
        pub2.publish(lanestatusMessage)

        key = cv2.waitKey(waitTime)  
        if verb > 2:
            print("Key pressed: {0}".format(str(key)))
        #if esc pressed, exit
        if key == 27 or key == 1048603:
            break
    cv2.destroyWindow("preview")

if __name__ == '__main__':
    verb = 0
    rosRate = 0.5
    waitTime = 1000
    if len(sys.argv) > 4:
        waitTime = int(sys.argv[4])

    if len(sys.argv) > 3:
        rosRate = float(sys.argv[3])

    if len(sys.argv) > 2:
        verb = int(sys.argv[2])

    if len(sys.argv) > 1:
        imgFile = sys.argv[1]
    else:
        print("Provide an image file to use as input")
        exit(0)
    
    try:
        readAndPublishVideo(verb, imgFile, rosRate, waitTime)
    except rospy.ROSInterruptException:
        pass  #allows for empty code in this except block without throwing error

