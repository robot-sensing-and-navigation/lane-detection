#!/usr/bin/env python3

import rospy
import cv2
import sys
from sensor_msgs.msg import Image
from geometry_msgs.msg import PoseArray
from std_msgs.msg import Bool
import scripts.lib.settings as settings
from cv_bridge import CvBridge
from message_filters import TimeSynchronizer, Subscriber


class overlay:
    def __init__(self):
        self.cvBridge = CvBridge()
        self.rawImage = None
        self.Camera = None
        self.rawLanes = None
        self.leftLane = None
        self.rightLane = None
        self.leftMostLane = None
        self.rightMostLane = None
        self.tracking = None
        self.laneChangingStatus = False
        self.roi = None
        self.image = None
        self.edgePoseData = None
        # TODO: Figure out if the flag is needed or ROS automatically handles this
        self.dataStreamFlag = False

# _______________________Callback functions to assign data values to member variables______________
    def rawImage_callback(self, frame):
        print("got an Image and ROI Information")
        self.rawImage = self.cvBridge.imgmsg_to_cv2(frame)
        # cv2.imshow("Frame", self.cvBridge.imgmsg_to_cv2(frame))
        # if cv2.waitKey(10) & 0xFF == ord('q'):
        #     cv2.destroyWindow("Overlay View")
        #     sys.exit()

    def edgePose_callback(self, edgePoseData):
        self.edgePoseData = self.cvBridge.imgmsg_to_cv2(edgePoseData)

    def roi_callback(self, laneCountInput):
        self.roi = settings.points_birdeye_transform["Lane_{}".format(laneCountInput)]

    def laneChangeCallback(self, laneChangingStatus):
        self.laneChangingStatus = laneChangingStatus
        pass

# _______________________Apply overlay method______________________________
    def apply_overlay(self):
        # TODO : Test this function, does not work outside debug mode

        cv2.imshow("Overlay View", self.rawImage)



        # cv2.putText(self.image, text='Look ahead Distance ', org=(10, 40), fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=1, color=(0, 0, 255), thickness=2, lineType=cv2.LINE_AA)
        # cv2.putText(self.image, text='Distance to left lane marker: ', org=(10, 100), fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=1, color=(0, 255, 255), thickness=2, lineType=cv2.LINE_AA)
        # cv2.putText(self.image, text='Distance to right lane marker: ', org=(700, 100), fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=1, color=(0, 255, 255), thickness=2, lineType=cv2.LINE_AA)

        if self.laneChangingStatus:
            cv2.putText(self.image, text='Changing lane', org=(10, 500), fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=2, color=(255, 255, 255), thickness=2, lineType=cv2.LINE_AA)
        else:
            cv2.putText(self.image, text='Staying in Lane', org=(10, 500), fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=2, color=(255, 255, 255), thickness=2, lineType=cv2.LINE_AA)
        if cv2.waitKey(10) & 0xFF == ord('q'):
            cv2.destroyAllWindows()
            sys.exit()
def main():
    rospy.init_node('overlay', anonymous=True)
    overlay_obj = overlay()
    # overlay_obj.imageFrame = rospy.Subscriber('CameraData', Image, overlay.rawImage_callback)
    overlay_obj.imageFrame = rospy.Subscriber('CameraData', Image, overlay_obj.rawImage_callback)
    # overlay_obj.roi = rospy.Subscriber('ROI', PoseArray, overlay.roi_callback)
    # overlay_obj.laneChangingStatus = rospy.Subscriber('LaneChanging', Bool, overlay_obj.laneChangeCallback)
    # # overlay_obj.rawLanes = rospy.Subscriber('RawLanes', PoseArray, callback)
    # # overlay_obj.leftLane = rospy.Subscriber('LeftLane', PoseArray, callback)
    # # overlay_obj.leftMostLane = rospy.Subscriber('LeftmostLane', PoseArray, callback)
    # # overlay_obj.rightMostLane = rospy.Subscriber('RightmostLane', PoseArray, callback)
    # # overlay_obj.rightLane = rospy.Subscriber('RightLane', PoseArray, callback)
    overlay_obj.laneChangingStatus = rospy.Subscriber('LaneChangeStatus', Bool, overlay_obj.laneChangeCallback)
    # TODO Test apply_overlay
    overlay_obj.apply_overlay()

    rospy.spin()


if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass
