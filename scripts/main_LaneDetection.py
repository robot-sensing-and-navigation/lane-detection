import cv2
from scripts.lib import settings
from scripts.lib import pipeline_lanefinding as lane_finding
from scripts.lib import pipeline_preprocessing as pre_processing
from scripts.lib.lib_photography import *
from scripts.lib import lib_perspective as perspective
from scripts.lib.lib_frame import *
import time

def main():
    '''Testing script for  perspective transform coupled with ED Line detection algo'''

    cap = cv2.VideoCapture("data/videos_test/Highway_iPhone.mp4")
    # cap = cv2.VideoCapture(0)
    out = cv2.VideoWriter('DocumentationResources/Videos/lanedetection.avi',
                          cv2.VideoWriter_fourcc(
                              'M', 'J', 'P', 'G'), settings.dst_frameRate,
                          (settings.ORIGINAL_SIZE[1], settings.ORIGINAL_SIZE[0]))

    preprocessing_obj1 = pre_processing.pipeline_preprocessing(settings.param_1)
    preprocessing_obj2 = pre_processing.pipeline_preprocessing(settings.param_2)
    perspective_obj = perspective.birdseye_perspective(4)

    lanedetection_obj = lane_finding.pipeline_lanefinding()
    count = 0
    while cap.isOpened():
        start_time = time.time()  # start timing
        # This line can be changed with read frames from another ros node
        ret, frame = cap.read()
        frame = rescale_frame(frame, 66.67)
        preprocessing_obj1.apply_pipeline(frame)
        perspective_obj.apply_skyview(frameHLS2RGB(preprocessing_obj1.frame_HLS))
        preprocessing_obj2.apply_pipeline(perspective_obj.frame_skyView)
        lanedetection_obj.apply_pipeline(frame, preprocessing_obj1, preprocessing_obj2)

        out.write(lanedetection_obj.dst_frame)
        cv2.imshow("LinesDrawn", lanedetection_obj.dst_frame)

        if cv2.waitKey(10) & 0xFF == ord('q'):
            break

    # mean preprocess time for each image
    cap.release()
    out.release()
    # cv2.destroyWindow("RGB FRAMES")  # destroy all opened windows
    cv2.destroyAllWindows()

    return 0


if __name__ == '__main__':
    main()

