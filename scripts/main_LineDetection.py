import cv2
from scripts.lib import settings
from scripts.lib import lib_perspective as perspective
from scripts.lib import lib_linedetection as linedetection
import time

'''https://answers.opencv.org/question/120277/default-color-space-for-frames-in-video/'''



def main():
    '''Testing script for  perspective transform coupled with ED Line detection algo'''

    cap = cv2.VideoCapture("data/videos_test/test_video_1.mp4")
    # cap = cv2.VideoCapture(0)
    out = cv2.VideoWriter('DocumentationResources/Videos/linedetection.avi',
                          cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), settings.dst_frameRate,
                          (settings.ORIGINAL_SIZE[1], settings.ORIGINAL_SIZE[0]))
    count = 0
    while cap.isOpened():
        start_time = time.time()  # start timing
        #This line can be changed with read frames from another ros node
        ret, frame = cap.read()

        cv2.imshow('RGB FRAMES', frame)

        perspective_obj = perspective.birdseye_perspective(4)
        perspective_obj.apply_skyview(frame)

        linedrawing_obj = linedetection.line_detection(perspective_obj.frame_skyView)
        # linedrawing_obj = linedetection.line_detection(frame);

        linedrawing_obj.Canny_Lines()
        linedrawing_obj.ED_Lines()

        out.write(linedrawing_obj.dst_frame)
        # cv2.imshow("LinesDrawn", linedrawing_obj.dst_frame)

        elapsed_time = time.time() - start_time  # end timing
        print('Frame Preprocess Complete: Time: {0:.3f}s'.format(elapsed_time))
        if cv2.waitKey(10) & 0xFF == ord('q'):
            break

    # mean preprocess time for each image
    cap.release()
    out.release()
    # cv2.destroyWindow("RGB FRAMES")  # destroy all opened windows
    cv2.destroyAllWindows()

    return 0


if __name__ == '__main__':
    main()
