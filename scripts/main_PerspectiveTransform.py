import cv2
from scripts.lib import settings
from scripts.lib import lib_perspective as perspective
import time
import numpy as np

'''https://answers.opencv.org/question/120277/default-color-space-for-frames-in-video/'''


def main():
    '''Testing script for video pre processing and perspective transform'''
    cap = cv2.VideoCapture("data/videos_test/iPhone_test_video720.avi")
    # cap = cv2.VideoCapture("data/videos_test/udacity_test_video_1.mp4")

    out = cv2.VideoWriter('DocumentationResources/Videos/output_PerspectiveTransform.mov',
                          cv2.VideoWriter_fourcc(
                              *'mp4v'), settings.dst_frameRate,
                          (960, 880))
    # cap = cv2.VideoCapture(0)
    count = 0
    while cap.isOpened():
        start_time = time.time()  # start timing
        # This line can be changed with read frames from another ros node
        ret, frame = cap.read()

        perspective_obj = perspective.birdseye_perspective(4)
        perspective_obj.apply_skyview(frame)
        # frame = cv2.circle(frame, (580, 460) , 2, color=(255, 255, 255), thickness=-1)
        frame = cv2.circle(frame, tuple(settings.points_birdeye_transform["Lane_4"]["source_points"][0].astype(int)), 10, color=(150, 255, 255), thickness=-1)
        frame = cv2.circle(frame, tuple(settings.points_birdeye_transform["Lane_4"]["source_points"][1].astype(int)), 10, color=(255, 100, 255), thickness=-1)
        frame = cv2.circle(frame, tuple(settings.points_birdeye_transform["Lane_4"]["source_points"][2].astype(int)), 10, color=(255, 255, 100), thickness=-1)
        frame = cv2.circle(frame, tuple(settings.points_birdeye_transform["Lane_4"]["source_points"][3].astype(int)), 10, color=(0, 255, 255), thickness=-1)

        perspective_obj.frame_skyView = cv2.circle(perspective_obj.frame_skyView, tuple(settings.points_birdeye_transform["Lane_4"]["destination_points"][0].astype(int)), 10, color=(150, 255, 255),
                                                   thickness=-1)
        perspective_obj.frame_skyView = cv2.circle(perspective_obj.frame_skyView, tuple(settings.points_birdeye_transform["Lane_4"]["destination_points"][1].astype(int)), 10, color=(255, 100, 255),
                                                   thickness=-1)
        perspective_obj.frame_skyView = cv2.circle(perspective_obj.frame_skyView, tuple(settings.points_birdeye_transform["Lane_4"]["destination_points"][2].astype(int)), 10, color=(255, 255, 100),
                                                   thickness=-1)
        perspective_obj.frame_skyView = cv2.circle(perspective_obj.frame_skyView, tuple(settings.points_birdeye_transform["Lane_4"]["destination_points"][3].astype(int)), 10, color=(0, 255, 255),
                                                   thickness=-1)
        cv2.imshow('RGB FRAMES', frame)
        cv2.imshow("Processed Image", perspective_obj.frame_skyView)
        finalFrame = np.concatenate((cv2.resize(frame, (960, 440)), cv2.resize(perspective_obj.frame_skyView, (960, 440))), axis=0)
        cv2.imshow("Concatenated Image", finalFrame)
        # out.write(frame)
        elapsed_time = time.time() - start_time  # end timing
        print('Frame Preprocess Complete: Time: {0:.3f}s'.format(elapsed_time))
        if cv2.waitKey(10) & 0xFF == ord('q'):
            break

    # mean preprocess time for each image
    cap.release()
    out.release()
    # cv2.destroyWindow("RGB FRAMES")  # destroy all opened windows
    cv2.destroyAllWindows()

    return 0


if __name__ == '__main__':
    main()