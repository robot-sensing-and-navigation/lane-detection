
import cv2
import numpy as np
from scripts.lib import settings
import matplotlib.pyplot as plt

class birdseye_perspective:
    def __init__(self, frame, lane_count):
        self.shape                 = settings.UNWARPED_SIZE
        self.roiPoints             = settings.points_birdeye_transform["Lane_"+str(lane_count)] #ROI Points keys defined as "Lane_<LaneCount>"
        self.birdeye_matrix        = cv2.getPerspectiveTransform(self.roiPoints["source_points"], self.roiPoints["destination_points"])
        self.inv_birdeye_matrix    = cv2.getPerspectiveTransform(self.roiPoints["destination_points"], self.roiPoints["source_points"])
        self.frame_skyView         = None
        self.frame_camView         = None

        #Only if dynamic ROI determination code is completed
        self.frame                 = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    #TODO
    def autoROI_Gen(self, THD_low=100, THD_high=100):
        edges = cv2.Canny(self.frame, THD_low, THD_high)
        plt.subplot(121), plt.imshow(self.frame, cmap='gray')
        plt.title('Original Image'), plt.xticks([]), plt.yticks([])
        plt.subplot(122), plt.imshow(edges, cmap='gray')
        plt.title('Edge Image'), plt.xticks([]), plt.yticks([])

