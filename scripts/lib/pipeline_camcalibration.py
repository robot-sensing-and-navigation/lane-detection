# -*- coding: utf-8 -*-
import os
import numpy
import cv2
import json


""" documentation & tutorials
https://docs.opencv.org/master/dc/dbb/tutorial_py_calibration.html
https://docs.opencv.org/2.4/modules/calib3d/doc/camera_calibration_and_3d_reconstruction.html
"""

"""
Load chessboards photos and find camera matrix and distorsion coefficient
Save camera calibration data into S7_cam_calibration.p for further usage
"""
#DONE
class pipeline_camcalibration:

    def __init__(self, frameList):
        """Constructor for pipeline
        @param frameList: List of checkerboard image paths for calibration file generation"""
        self.frameList              = frameList

    def gen_CalibParams(self, criteria, patternShape):
        """Generates Camera calibration matrices and stores them in JSON format
        @param criteria: Termination criteria of form cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, iters, tolerance
        @param patternShape: Row x Column count for checkerboard pattern
        @returns: None
        """
        criteria               = criteria
        patternShape           = patternShape
        # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
        objp = numpy.zeros((numpy.prod(patternShape), 3), numpy.float32)  # 9*6 Defined using chessboard cell count
        objp[:, :2] = numpy.mgrid[0:patternShape[0], 0:patternShape[1]].T.reshape(-1, 2)  # -1 is used to infer the dimension of resulting array and automatically reshape it

        # Arrays to store object points and image points from all the images.
        objpoints = []  # 3d point in real world space
        imgpoints = []  # 2d points in image plane.

        for filename_ in self.frameList:
            img = cv2.imread(filename_)
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

            cv2.imshow("Chessboard", gray)
            cv2.waitKey(200)
            cv2.destroyAllWindows()

            # Find the chess board corners
            ret, corners = cv2.findChessboardCorners(gray, tuple(patternShape), None)

            # If found, add object points, image points (after refining them)
            if ret == True:
                print('Corners Found')
                print(filename_)
                objpoints.append(objp)
                corners2 = cv2.cornerSubPix(gray, corners, (11, 11), (-1, -1), criteria)
                imgpoints.append(corners)

                # Draw and display the corners
                cv2.drawChessboardCorners(img, tuple(patternShape), corners2, ret)
                cv2.imshow('Corner marked chessboard', img)
                file_name = os.path.splitext(os.path.basename(filename_))[0] + '_out.jpg'
                cv2.imwrite('data/camCalibration/output_chessboard/' + file_name, img)
                cv2.waitKey(300)
        cv2.destroyAllWindows()

        # Cam matrix and distortion coeff
        ret, camera_matrix, distortion_coefficient, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1], None, None)
        calibrationConfig = {"camera_matrix": camera_matrix.tolist(),
                             "distortion_coefficient": distortion_coefficient.tolist()}

        with open("data/camCalibration/calibrationConfig_/CalibrationConfig.json", 'w', encoding="utf8") as outfile:
            json.dump(calibrationConfig, outfile)
        print("JSON File created")

    def apply_undistort(self, frame_BGR, camera_matrix, distortion_coeff):
        """Receives a cv.mat object and applies a distortion correction based on camera params
        @param frame_BGR: distorted BGR cv.mat object
        @param camera_matrix: Camera perspective correction matrix from JSON file
        @param distortion_coeff: Camera distortion coefficient vector from JSON file
        @return: frame_undistort_BGR-> cv.mat type array of a corrected image"""

        H,  W                   = frame_BGR.shape[:2]
        new_camera_matrix, ZoI  = cv2.getOptimalNewCameraMatrix(camera_matrix, distortion_coeff, (W, H), 1, (W, H))
        # undistort
        frame_undistort_BGR         = cv2.undistort(frame_BGR, camera_matrix, distortion_coeff, None, new_camera_matrix)
        # crop the image
        xi, yi, wi, hi          = ZoI
        frame_undistort_BGR    = frame_undistort_BGR[yi : yi+hi, xi : xi+wi]
        return frame_undistort_BGR


