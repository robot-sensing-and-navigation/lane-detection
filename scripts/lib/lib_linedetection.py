import cv2
from pylsd2 import LineSegmentDetection, LineSegmentDetectionED
import matplotlib.pyplot as plt


class line_detection:
    def __init__(self, frame):
        self.source_frame = frame
        # self.frameGray = cv2.cvtColor(frame_RGB, cv2.COLOR_BGR2GRAY)
        self.lines = None
        self.dst_frame = frame

    def ED_Lines(self, grad_thresh=80, anchor_thresh=2, scan_interval=2, line_fit_err_thresh=1.4):
        self.lines = LineSegmentDetectionED(
            self.source_frame, grad_thres=grad_thresh, anchor_thres=anchor_thresh,
            scan_interval=scan_interval, line_fit_err_thres=line_fit_err_thresh)

        for l in self.lines:
            line_start, line_end = tuple(l[:2]), tuple(l[2:4])
            cv2.line(self.dst_frame, line_start, line_end, (0, 0, 255), 1)

    def Canny_Lines(self, THD_low=100, THD_high=100):
        edges = cv2.Canny(self.source_frame, THD_low, THD_high)
        edges = cv2.cvtColor(edges, cv2.COLOR_GRAY2RGB)
        # Weighted sum of both arrays to overlay canny lines
        self.dst_frame = cv2.addWeighted(
            self.source_frame, 1.0, edges, 0.5, 0.)

    def apply_pipeline(self):
        return self
