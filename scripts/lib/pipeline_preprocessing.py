# import libraries
from scripts.lib.lib_frame import *
from scripts.lib.lib_photography import *
from scripts.lib import lib_linedetection as linedetection
from scripts.lib import settings
import numpy as np
import copy
import cv2

# TODO ABHINAV


class pipeline_preprocessing:
    def __init__(self, param):
        ''' Constructor for the preprocessing class
         @param frame_RGB: RGB input frame
         @param param: Image processing params (e.g. binary threshold, exposure value, contrast etc. )
         '''

        self.thd_S_mag = param['thd_S_mag']
        self.thd_S_arg = param['thd_S_arg']
        self.thd_S_x = param['thd_S_x']

        self.thd_L_mag = param['thd_L_mag']
        self.thd_L_arg = param['thd_L_arg']
        self.thd_L_y = param['thd_L_y']

        self.thd_L_shadow = param['thd_shadow_L']
        self.thd_S_shadow = param['thd_shadow_S']
        self.thd_L_highlight = param['thd_highlight_L']
        self.thd_S_highlight = param['thd_highlight_S']
        self.frame_RGB = None
        self.frame_HLS = None

    def colorspace_conversion(self, frame, HLS=False):
        if HLS == False:
            self.frame_RGB = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            self.frame_HLS = cv2.cvtColor(self.frame_RGB, cv2.COLOR_RGB2HLS)
        else:
            self.frame_RGB = cv2.cvtColor(frame, cv2.COLOR_HLS2RGB)
            self.frame_HLS = frame

    def apply_white_balance(self):
        """
        White balancing of self.frame_HLS
        @param self.frame_HLS -> HLS converted RGB frame
        @returns: None
        """
        self.frame_HLS = frame_HLS_balance_white(self.frame_HLS)

    def apply_exposure_balance(self):
        """
        Exposure balancing of self.frame_HLS
        @param self.frame_HLS -> HLS converted RGB frame
        @returns: None
        """
        self.frame_HLS = frame_HLS_balance_exposure(self.frame_HLS)

    def apply_highlight_remove(self):
        """
        Highlight removal of self.frame_HLS
        Removal of pixel with low saturation and high lightness
        @param self.frame_HLS -> HLS converted RGB frame
        @returns: None
        """
        L_cond = self.frame_HLS[:, :, 1] > self.thd_L_highlight
        S_cond = self.frame_HLS[:, :, 2] < self.thd_S_shadow
        """we wont keep pixel with low saturation and high lightness"""
        LS_cond = (L_cond & S_cond)
        self.frame_HLS[LS_cond, :] = [0, 0, 0]

    def apply_shadow_remove(self):
        """
        Shadow removal of self.frame_HLS
        @param self.frame_HLS -> HLS converted RGB frame
        @returns: None
        """
        L_cond = self.frame_HLS[:, :, 1] < self.thd_L_shadow
        S_cond = self.frame_HLS[:, :, 2] > self.thd_S_shadow
        """we wont keep pixel with high saturation and low lightness"""
        LS_cond = (L_cond & S_cond)
        self.frame_HLS[LS_cond, :] = [0, 0, 0]

    def apply_sobel(self):
        """
        Application of sobel filter to self.frame_HLS
        @param self.frame_HLS -> HLS converted RGB frame
        @returns: White balanced HLS frame
        """
        # 1st degree sobel operations
        # self.sobel_H_x, self.sobel_H_y, self.sobel_H_mag, self.sobel_H_arg = frame_sobel(self.frame_HLS[:,:,0])
        self.sobel_L_x, self.sobel_L_y, self.sobel_L_mag, self.sobel_L_arg = frame_sobel(
            self.frame_HLS[:, :, 1])
        self.sobel_S_x, self.sobel_S_y, self.sobel_S_mag, self.sobel_S_arg = frame_sobel(
            self.frame_HLS[:, :, 2])

    def apply_sobel2(self):
        """
        Application of second degree sobel filter to self.frame_HLS
        @param self.frame_HLS -> HLS converted RGB frame
        @returns: White balanced HLS frame
        """

        # self.sobel2_H_x, self.sobel2_H_y, self.sobel2_H_mag, self.sobel2_H_arg = frame_sobel(self.sobel_H_mag)
        self.sobel2_L_x, self.sobel2_L_y, self.sobel2_L_mag, self.sobel2_L_arg = frame_sobel(
            self.sobel_L_x)
        self.sobel2_S_x, self.sobel2_S_y, self.sobel2_S_mag, self.sobel2_S_arg = frame_sobel(
            self.sobel_S_x)

    def apply_sobel_mask(self):
        """apply filter on sobel magnitude of L and S chanels"""
        L_mag_cond = numpy.abs(
            127 - self.sobel_L_mag.astype(numpy.int)) > self.thd_L_mag
        S_mag_cond = numpy.abs(
            127 - self.sobel_S_mag.astype(numpy.int)) > self.thd_S_mag
        LS_mag_cond = L_mag_cond | S_mag_cond
        self.mask_LS_mag = numpy.zeros_like(LS_mag_cond, dtype=numpy.uint8)
        self.mask_LS_mag[LS_mag_cond] = 1

        """we keep pixel with sobel_S_arg far from scaled-127 = unscaled-90deg"""
        Larg_cond = numpy.abs(
            127 - self.sobel_L_arg.astype(numpy.int)) > self.thd_L_arg
        Sarg_cond = numpy.abs(
            127 - self.sobel_S_arg.astype(numpy.int)) > self.thd_S_arg
        LS_arg_cond = Larg_cond & Sarg_cond
        self.mask_LS_Larg = numpy.zeros_like(LS_arg_cond, dtype=numpy.uint8)
        self.mask_LS_Larg[LS_arg_cond] = 1

        """we remove pixels with high Ly """
        Ly_cond = numpy.abs(
            127 - self.sobel_L_y.astype(numpy.int)) < self.thd_L_y
        self.mask_Ly = numpy.zeros_like(LS_arg_cond, dtype=numpy.uint8)
        self.mask_Ly[Ly_cond] = 1

    def ed_lines(self):
        linedrawing_obj = linedetection.line_detection(
            copy.deepcopy(self.frame_HLS[:, :, 1]))

        linedrawing_obj.ED_Lines(
            grad_thresh=80, anchor_thresh=20, scan_interval=2, line_fit_err_thresh=14)

        linedrawing_obj_2 = linedetection.line_detection(
            copy.deepcopy(self.frame_HLS[:, :, 2]))

        linedrawing_obj_2.ED_Lines(
            grad_thresh=80, anchor_thresh=20, scan_interval=2, line_fit_err_thresh=14)

        return linedrawing_obj.lines, linedrawing_obj_2.lines

    def apply_pipeline(self, frame, HLS=False):

        self.colorspace_conversion(frame, HLS)
        # cv2.imshow("initial image", self.frame_HLS)

        self.apply_exposure_balance()
        self.apply_white_balance()
        # cv2.imshow("before sobel", self.frame_HLS)

        self.apply_sobel()
        # cv2.imshow("after sobel", self.frame_HLS)

        # remove highlights and shadows
        self.apply_highlight_remove()
        self.apply_shadow_remove()
        # cv2.imshow("after highlight shadow remove", self.frame_HLS)

        self.apply_sobel_mask()

        # create filter mask
        mask_note = self.mask_LS_mag + self.mask_LS_Larg + self.mask_Ly

        self.frame_HLS[~(mask_note >= 3)] = [0, 0, 0]
        # cv2.imshow("after sobel mask", self.frame_HLS)

        # self.frame_HLS = gaussian_blur(self.frame_HLS, 5)

    def apply_pipeline_noSobel(self, frame, apply_mask=False, HLS=False):
        self.colorspace_conversion(frame, HLS)
        self.apply_exposure_balance()
        self.apply_white_balance()

        if apply_mask == True:

            mask = np.zeros(
                (frame.shape[0], frame.shape[1]), dtype=numpy.uint8)

            lines_1, lines_2 = self.ed_lines()
            for l in lines_1:
                line_start, line_end = tuple(l[:2]), tuple(l[2:4])

                m = abs(np.rad2deg(np.arctan2(
                    (line_end[1] - line_start[1]), (line_end[0] - line_start[0]))))
                if m > 10 and m <= 170:
                    cv2.line(mask, line_start, line_end, (255, 255, 255), 1)

            for l in lines_2:
                line_start, line_end = tuple(l[:2]), tuple(l[2:4])
                m = abs(np.rad2deg(np.arctan2(
                    (line_end[1] - line_start[1]), (line_end[0] - line_start[0]))))
                if m > 10 and m <= 170:
                    cv2.line(mask, line_start, line_end, (255, 255, 255), 1)

            # cv2.imshow("before ed lines mask", self.frame_HLS)

            self.apply_highlight_remove()
            self.apply_shadow_remove()

            for i in range(mask.shape[0] - 1):
                for j in range(mask.shape[1] - 1):
                    if mask[i][j] == 0:
                        self.frame_HLS[i][j] = [0, 0, 0]

            # cv2.imshow("after ed lines mask", self.frame_HLS)
