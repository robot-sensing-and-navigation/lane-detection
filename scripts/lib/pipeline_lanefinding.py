from scripts import main_Preprocessing
from scripts.lib import pipeline_preprocessing as pre_processing
from scripts.lib import lib_perspective as perspective
from scripts.lib import lib_linedetection as linedetection
from scripts.lib import settings
import cv2
import time
from copy import deepcopy

from scripts.lib.lib_frame import *
from scripts.lib.lib_photography import *
from scripts.lib.lib_curve_slider import curve_slider


class pipeline_lanefinding:

    def __init__(self):
        self.dst_frame = None

        pass

    def apply_pipeline(self, frame_RGB_origin, preprocessing_obj1, preprocessing_obj2, perspective_obj1, perspective_obj2):
        start_time = time.time()
        frame_RGB_skyview = perspective_obj1.frame_skyView
        frame_gray_skyview = frameRGB2gray(frame_RGB_skyview)
        frame_binary_skyview = numpy.zeros_like(
            frame_gray_skyview, dtype=numpy.uint8)
        frame_binary_skyview[frame_gray_skyview > 0] = 255
        # cv2.imshow('frame binary first', frame_binary_skyview)

        # frame_HLS[~(frame_mask_ZoI(frame_HLS, self.points_ZoI) > 0)] = [
        #     0, 0, 0]
        # frame_RGB_skyview_preprocessed = self.class_birdseye.apply_skyview(
        #     frameHLS2RGB(frame_HLS))

        frame_gray_2nd = frameRGB2gray(preprocessing_obj2.frame_HLS)
        frame_binary_2nd = numpy.zeros_like(frame_gray_2nd, dtype=numpy.uint8)
        frame_binary_2nd[frame_gray_2nd > 0] = 255
        # cv2.imshow('frame binary second', frame_binary_2nd)

        # Curve slider
        curve_slider_obj = curve_slider(settings.param_curve_class)
        curve_slider_obj.windows_slide(frame_binary_2nd)

        elapsed_time = time.time() - start_time  # end timing
        print('Frame Preprocess Complete: Time: {0:.3f}s'.format(
            elapsed_time))

        # Project detected lines in origin frame
        frame_RGB_skyview_CurrentLane = numpy.zeros_like(frame_RGB_origin)
        frame_RGB_skyview_CurrentLane = frame_RGB_draw_curve(
            frame_RGB_skyview_CurrentLane, curve_slider_obj.coeff_L, curve_slider_obj.xy_L_start, curve_slider_obj.xy_L_end, [255, 0, 50])
        frame_RGB_skyview_CurrentLane = frame_RGB_draw_curve(
            frame_RGB_skyview_CurrentLane, curve_slider_obj.coeff_R, curve_slider_obj.xy_R_start, curve_slider_obj.xy_R_end, [0, 0, 255])

        frame_RGB_skyview_NextLane = numpy.zeros_like(frame_RGB_origin)
        frame_RGB_skyview_NextLane = frame_RGB_draw_curve(
            frame_RGB_skyview_NextLane, curve_slider_obj.coeff_next_L, curve_slider_obj.xy_next_L_start, curve_slider_obj.xy_next_L_end, [255, 255, 0])
        frame_RGB_skframe_RGB_skyview_CurrentLane = numpy.zeros_like(
            frame_RGB_origin)
        frame_RGB_skyview_CurrentLane = frame_RGB_draw_curve(
            frame_RGB_skyview_CurrentLane, curve_slider_obj.coeff_L, curve_slider_obj.xy_L_start, curve_slider_obj.xy_L_end, [255, 0, 50])
        frame_RGB_skyview_CurrentLane = frame_RGB_draw_curve(
            frame_RGB_skyview_CurrentLane, curve_slider_obj.coeff_R, curve_slider_obj.xy_R_start, curve_slider_obj.xy_R_end, [0, 0, 255])

        frame_RGB_skyview_NextLane = numpy.zeros_like(frame_RGB_origin)
        frame_RGB_skyview_NextLane = frame_RGB_draw_curve(
            frame_RGB_skyview_NextLane, curve_slider_obj.coeff_next_L, curve_slider_obj.xy_next_L_start, curve_slider_obj.xy_next_L_end, [255, 255, 0])
        frame_RGB_skyview_NextLane = frame_RGB_draw_curve(
            frame_RGB_skyview_NextLane, curve_slider_obj.coeff_next_R, curve_slider_obj.xy_next_R_start, curve_slider_obj.xy_next_R_end, [0, 255, 255])

        # Reversed birdseye view transform
        frame_RGB_camview_CurrentLane = perspective_obj1.apply_vehicleview(
            frame_RGB_skyview_CurrentLane)
        frame_RGB_camview_NextLane = perspective_obj1.apply_vehicleview(
            frame_RGB_skyview_NextLane)

        frame_mix_RGB_origin_Lane = cv2.addWeighted(
            frame_RGB_origin, 0.9, frame_RGB_camview_NextLane, 1, 0)
        frame_mix_RGB_origin_Lane = cv2.addWeighted(
            frame_mix_RGB_origin_Lane, 0.9, frame_RGB_camview_CurrentLane, 1, 0)

        frame_RGB_ed_lines = perspective_obj2.frame_camView

        """make verbose frame"""
        # frame_mix_RGB_origin_Lane_verbose = self.make_frame_verbose(
        #     preprocessing_obj, frame_mix_RGB_origin_Lane, frame_RGB_skyview, frame_binary_skyview)
        self.dst_frame = self.make_frame_verbose(
            preprocessing_obj1, frame_mix_RGB_origin_Lane, frame_RGB_skyview, frame_binary_skyview, frame_RGB_ed_lines, curve_slider_obj)

    def make_frame_verbose(self, preprocessing, frame_mix_RGB_origin_Lane, frame_RGB_skyview, frame_binary_skyview, frame_RGB_ed_lines, curve_slider):  # curve_slider_obj
        """make verbose frame"""
        frame_mix_RGB_origin_Lane_verbose = numpy.copy(
            frame_mix_RGB_origin_Lane)
        W = int(frame_mix_RGB_origin_Lane_verbose.shape[1]/4)
        H = int(frame_mix_RGB_origin_Lane_verbose.shape[0]/4)
        # print(H, W)
        frame_RGB_skyview_resized = cv2.resize(
            frame_RGB_skyview, (W, H), interpolation=cv2.INTER_AREA)
        frame_skyview_resized = frame_scale_uint8(cv2.resize(
            frame_binary_skyview, (W, H), interpolation=cv2.INTER_AREA))
        frame_binary_2_resized = frame_scale_uint8(cv2.resize(
            curve_slider.frame_binary, (W, H), interpolation=cv2.INTER_AREA))
        frame_binary_2_resized = numpy.dstack(
            (frame_binary_2_resized, frame_binary_2_resized, frame_binary_2_resized))
        frame_RGB_ed_lines_resized = frame_scale_uint8(cv2.resize(
            frame_RGB_ed_lines, (W, H), interpolation=cv2.INTER_AREA))
        frame_RBG_skyview_resized = numpy.dstack(
            (frame_skyview_resized, frame_skyview_resized, frame_skyview_resized))
        frame_HLS_resized = cv2.resize(
            preprocessing.frame_HLS, (W, H), interpolation=cv2.INTER_AREA)
        frame_RGB_draw_windows_resized = cv2.resize(
            curve_slider.frame_RGB_draw_windows, (W, H), interpolation=cv2.INTER_AREA)
        frame_RGB_draw_windows_resized = cv2.addWeighted(
            frame_RGB_draw_windows_resized, 0.9, frame_binary_2_resized, 1, 0)

        # frame_RGB_draw_windows_unsized      = cv2.addWeighted(curve_slider_obj.frame_RGB_draw_windows, 0.9, numpy.dstack((frame_scale_uint8(frame_binary_skyview), frame_scale_uint8(frame_binary_skyview), frame_scale_uint8(frame_binary_skyview))), 0.2, 0)
        # frame_show(frame_RGB_draw_windows_unsized , title = 'frame_RGB_draw_windows_unsized')
        # frame_mix_RGB_origin_Lane
        frame_mix_RGB_origin_Lane_verbose[:H, :W] = frame_HLS_resized
        frame_mix_RGB_origin_Lane_verbose[:H,
        W:2*W] = frame_RBG_skyview_resized
        frame_mix_RGB_origin_Lane_verbose[:H,
        2*W:3*W] = frame_RGB_ed_lines_resized
        frame_mix_RGB_origin_Lane_verbose[:H, 3 *
                                              W:4*W] = frame_RGB_draw_windows_resized

        """add text to verbose frame"""
        # coeff in ego vehicle's coordination system, in pixel dimension
        coeff_L_from_vehicle_birdview_px = curve_slider.coeff_from_vehicle_birdview(
            curve_slider.coeff_L)
        coeff_R_from_vehicle_birdview_px = curve_slider.coeff_from_vehicle_birdview(
            curve_slider.coeff_R)
        coeff_next_L_from_vehicle_birdview_px = curve_slider.coeff_from_vehicle_birdview(
            curve_slider.coeff_next_L)
        coeff_next_R_from_vehicle_birdview_px = curve_slider.coeff_from_vehicle_birdview(
            curve_slider.coeff_next_R)

        # coeff in ego vehicle's coordination system, in meter dimension
        xm_by_pixel = curve_slider.xm_by_pixel
        ym_by_pixel = curve_slider.ym_by_pixel
        coeff_L_from_vehicle_birdview = curve_slider.coeff_pixel_to_meter(
            coeff_L_from_vehicle_birdview_px, xm_by_pixel, ym_by_pixel)
        coeff_R_from_vehicle_birdview = curve_slider.coeff_pixel_to_meter(
            coeff_R_from_vehicle_birdview_px, xm_by_pixel, ym_by_pixel)
        coeff_next_L_from_vehicle_birdview = curve_slider.coeff_pixel_to_meter(
            coeff_next_L_from_vehicle_birdview_px, xm_by_pixel, ym_by_pixel)
        coeff_next_R_from_vehicle_birdview = curve_slider.coeff_pixel_to_meter(
            coeff_next_R_from_vehicle_birdview_px, xm_by_pixel, ym_by_pixel)

        # calculate heading_angle, curvature and space-left-in-lane in meter dimension
        m_look_ahead = curve_slider.m_look_ahead
        heading_angle_L, curvature_L, space_left_in_lane_L = curve_slider.coeff_SLAM(
            coeff_L_from_vehicle_birdview, m_look_ahead)
        heading_angle_R, curvature_R, space_left_in_lane_R = curve_slider.coeff_SLAM(
            coeff_R_from_vehicle_birdview, m_look_ahead)

        # add text to verbose frame, curve polynomial
        coeff_L_text = [str("{:.2e}".format(x))
                        for x in coeff_L_from_vehicle_birdview]
        text_L = ' | '.join(coeff_L_text)
        coeff_R_text = [str("{:.2e}".format(x))
                        for x in coeff_R_from_vehicle_birdview]
        text_R = ' | '.join(coeff_R_text)
        coeff_next_L_text = [str("{:.2e}".format(x))
                             for x in coeff_next_L_from_vehicle_birdview]
        text_next_L = ' | '.join(coeff_next_L_text)
        coeff_next_R_text = [str("{:.2e}".format(x))
                             for x in coeff_next_R_from_vehicle_birdview]
        text_next_R = ' | '.join(coeff_next_R_text)

        lanechange_color = (255, 255, 255)
        lanechange_text = curve_slider.lanechange
        if curve_slider.lanechange == 'Left to Right':
            lanechange_color = (0, 0, 255)
            lanechange_text = lanechange_text + '>>>>>>>>>>>>>>'
        if curve_slider.lanechange == 'Right to Left':
            lanechange_color = (255, 0, 50)
            lanechange_text = '<<<<<<<<<<<<<<<<<<' + lanechange_text

        cv2.putText(frame_mix_RGB_origin_Lane_verbose, 'Lane Change || ' + lanechange_text,
                    (400, 350), cv2.FONT_HERSHEY_COMPLEX, 0.75, lanechange_color, 1, cv2.LINE_AA)

        cv2.putText(frame_mix_RGB_origin_Lane_verbose, 'Left Poly | ' + text_L,
                    (100, 270), cv2.FONT_HERSHEY_COMPLEX, 0.5, (255, 0, 50), 1, cv2.LINE_AA)
        cv2.putText(frame_mix_RGB_origin_Lane_verbose, 'Left Type | ' + str(curve_slider.linetype_L),
                    (100, 290), cv2.FONT_HERSHEY_COMPLEX, 0.5, (255, 0, 50), 1, cv2.LINE_AA)
        cv2.putText(frame_mix_RGB_origin_Lane_verbose, 'Left Confindex | ' + str(curve_slider.confindex_L),
                    (100, 310), cv2.FONT_HERSHEY_COMPLEX, 0.5, (255, 0, 50), 1, cv2.LINE_AA)

        cv2.putText(frame_mix_RGB_origin_Lane_verbose, 'Right Poly | ' + text_R,
                    (720, 270), cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 0, 255), 1, cv2.LINE_AA)
        cv2.putText(frame_mix_RGB_origin_Lane_verbose, 'Right Type | ' + str(curve_slider.linetype_R),
                    (720, 290), cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 0, 255), 1, cv2.LINE_AA)
        cv2.putText(frame_mix_RGB_origin_Lane_verbose, 'Right Confindex | ' + str(curve_slider.confindex_R),
                    (720, 310), cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 0, 255), 1, cv2.LINE_AA)

        cv2.putText(frame_mix_RGB_origin_Lane_verbose, text_next_L, (20, 200),
                    cv2.FONT_HERSHEY_COMPLEX, 0.5, (255, 255, 0), 1, cv2.LINE_AA)
        cv2.putText(frame_mix_RGB_origin_Lane_verbose, 'Next Left Type | ' + str(curve_slider.linetype_next_L),
                    (20, 220), cv2.FONT_HERSHEY_COMPLEX, 0.5, (255, 255, 0), 1, cv2.LINE_AA)
        cv2.putText(frame_mix_RGB_origin_Lane_verbose, 'Next Left Confindex | ' + str(curve_slider.confindex_next_L),
                    (20, 240), cv2.FONT_HERSHEY_COMPLEX, 0.5, (255, 255, 0), 1, cv2.LINE_AA)

        cv2.putText(frame_mix_RGB_origin_Lane_verbose, text_next_R, (820, 200),
                    cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 255, 255), 1, cv2.LINE_AA)
        cv2.putText(frame_mix_RGB_origin_Lane_verbose, 'Next Right Type | ' + str(curve_slider.linetype_next_R),
                    (820, 220), cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 255, 255), 1, cv2.LINE_AA)
        cv2.putText(frame_mix_RGB_origin_Lane_verbose, 'Next Right Confindex | ' + str(curve_slider.confindex_next_R),
                    (820, 240), cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 255, 255), 1, cv2.LINE_AA)

        # add text to verbose frame, heading_angle, curvature and space-left-in-lane in meter dimension
        heading_angle_L_text = str("{:.2e}".format(heading_angle_L))
        heading_angle_R_text = str("{:.2e}".format(heading_angle_R))
        curvature_L_text = str("{:.2e}".format(curvature_L))
        curvature_R_text = str("{:.2e}".format(curvature_R))
        space_left_in_lane_L_text = str("{:.2e}".format(space_left_in_lane_L))
        space_left_in_lane_R_text = str("{:.2e}".format(space_left_in_lane_R))

        cv2.putText(frame_mix_RGB_origin_Lane_verbose, '_____________________',
                    (20, 350), cv2.FONT_HERSHEY_COMPLEX, 0.5, (255, 255, 255), 1, cv2.LINE_AA)
        cv2.putText(frame_mix_RGB_origin_Lane_verbose, 'Look ahead distance | ' + str(m_look_ahead) +
                    ' (m)', (20, 370), cv2.FONT_HERSHEY_COMPLEX, 0.5, (255, 255, 255), 1, cv2.LINE_AA)
        cv2.putText(frame_mix_RGB_origin_Lane_verbose, 'Heading angle Left | ' + heading_angle_L_text +
                    ' (deg)', (20, 390), cv2.FONT_HERSHEY_COMPLEX, 0.5, (255, 255, 255), 1, cv2.LINE_AA)
        cv2.putText(frame_mix_RGB_origin_Lane_verbose, 'Curvature Left | ' + curvature_L_text +
                    ' (1/m)', (20, 410), cv2.FONT_HERSHEY_COMPLEX, 0.5, (255, 255, 255), 1, cv2.LINE_AA)
        cv2.putText(frame_mix_RGB_origin_Lane_verbose, 'Space-left-in-lane Left | ' + space_left_in_lane_L_text +
                    ' (m)', (20, 430), cv2.FONT_HERSHEY_COMPLEX, 0.5, (255, 255, 255), 1, cv2.LINE_AA)

        cv2.putText(frame_mix_RGB_origin_Lane_verbose, '_____________________',
                    (820, 350), cv2.FONT_HERSHEY_COMPLEX, 0.5, (255, 255, 255), 1, cv2.LINE_AA)
        cv2.putText(frame_mix_RGB_origin_Lane_verbose, 'Look ahead distance | ' + str(m_look_ahead) +
                    ' (m)', (820, 370), cv2.FONT_HERSHEY_COMPLEX, 0.5, (255, 255, 255), 1, cv2.LINE_AA)
        cv2.putText(frame_mix_RGB_origin_Lane_verbose, 'Heading angle Right | ' + heading_angle_R_text +
                    ' (deg)', (820, 390), cv2.FONT_HERSHEY_COMPLEX, 0.5, (255, 255, 255), 1, cv2.LINE_AA)
        cv2.putText(frame_mix_RGB_origin_Lane_verbose, 'Curvature Right | ' + curvature_R_text +
                    ' (1/m)', (820, 410), cv2.FONT_HERSHEY_COMPLEX, 0.5, (255, 255, 255), 1, cv2.LINE_AA)
        cv2.putText(frame_mix_RGB_origin_Lane_verbose, 'Space-left-in-lane Right | ' + space_left_in_lane_R_text +
                    ' (m)', (820, 430), cv2.FONT_HERSHEY_COMPLEX, 0.5, (255, 255, 255), 1, cv2.LINE_AA)

        # Add context of lane merging or divergence
        cv2.putText(frame_mix_RGB_origin_Lane_verbose, 'Merging || Diverging', (150, 625), cv2.FONT_HERSHEY_COMPLEX, 0.5, (255, 255, 255), 1, cv2.LINE_AA)
        if not curve_slider.laneMerge_L:
            cv2.putText(frame_mix_RGB_origin_Lane_verbose, str(curve_slider.laneMerge_L), (170, 650), cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 0, 255), 1, cv2.LINE_AA)
        else:
            cv2.putText(frame_mix_RGB_origin_Lane_verbose, str(curve_slider.laneMerge_L), (170, 650), cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 255, 0), 1, cv2.LINE_AA)
        if not curve_slider.laneDiverge_L:
            cv2.putText(frame_mix_RGB_origin_Lane_verbose, str(curve_slider.laneDiverge_L), (240, 650), cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 0, 255), 1, cv2.LINE_AA)
        else:
            cv2.putText(frame_mix_RGB_origin_Lane_verbose, str(curve_slider.laneDiverge_L), (240, 650), cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 255, 0), 1, cv2.LINE_AA)

        cv2.putText(frame_mix_RGB_origin_Lane_verbose, 'Merging || Diverging', (860, 625), cv2.FONT_HERSHEY_COMPLEX, 0.5, (255, 255, 255), 1, cv2.LINE_AA)
        if not curve_slider.laneMerge_R:
            cv2.putText(frame_mix_RGB_origin_Lane_verbose, str(curve_slider.laneMerge_R), (880, 650), cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 0, 255), 1, cv2.LINE_AA)
        else:
            cv2.putText(frame_mix_RGB_origin_Lane_verbose, str(curve_slider.laneMerge_R), (880, 650), cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 255, 0), 1, cv2.LINE_AA)
        if not curve_slider.laneDiverge_R:
            cv2.putText(frame_mix_RGB_origin_Lane_verbose, str(curve_slider.laneDiverge_R), (950, 650), cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 0, 255), 1, cv2.LINE_AA)
        else:
            cv2.putText(frame_mix_RGB_origin_Lane_verbose, str(curve_slider.laneDiverge_R), (950, 650), cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 255, 0), 1, cv2.LINE_AA)


        return frame_mix_RGB_origin_Lane_verbose
