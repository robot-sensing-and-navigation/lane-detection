import numpy


# TODO CHANGE FOR IPHONE OR ORIGINAL DATASET
ORIGINAL_SIZE = [720, 1280]  # Resolution of Camera
UNWARPED_SIZE = [720, 1280]  # Calibrated frame resolution

# ORIGINAL_SIZE = [1080, 19200]  # Resolution of Camera
# UNWARPED_SIZE = [1080, 1920]  # Calibrated frame resolution
# # TODO: CHANGE FOR KITTI
# ORIGINAL_SIZE = [370, 1226]  # Resolution of Camera
# UNWARPED_SIZE = [370, 1226]  # Resolution of Camera


dst_frameRate = 30

"""PreProcessing parameter sets for different cases"""
param_1 = {'thd_highlight_L': 150,  # mid L         # low-pass filter on H-chanel
           'thd_highlight_S': 30,  # low S         # low-pass filter on H-chanel
           'thd_shadow_L': 30,  # low L
           'thd_shadow_S': 50,  # high S
           'thd_S_mag': 10,  # high S_mag 25
           'thd_S_arg': 25,  # high S_arg 25
           'thd_S_x': 0,  # high-pass filter on sobel of L-chanel in direction x, by defaut 35
           'thd_L_mag': 20,  # high L_mag 25   # high-pass filter on magnitude of sobel of L-chanel, by defaut 5
           'thd_L_arg': 0,
           'thd_L_y': 75}

param_2 = {'thd_highlight_L': 150,  # 55mid L     # low-pass filter on H-chanel
           'thd_highlight_S': 30,  # 25low S      # low-pass filter on H-chanel
           'thd_shadow_L': 30,  # 25low L
           'thd_shadow_S': 50,  # high S
           'thd_S_mag': 25,  # high S_mag 25
           'thd_S_arg': 0,  # high S_arg 25
           'thd_S_x': 0,  # high-pass filter on sobel of L-chanel in direction x, by defaut 35
           'thd_L_mag': 20,  # high-pass filter on magnitude of sobel of L-chanel, by defaut 5
           'thd_L_arg': 100,  # 127
           'thd_L_y': 100}  # low-pass filter on scaled argument of sobel vector of L-chanel, by defaut 200 == 70 degree

#TODO TEST HOW MUCH MORE THIS CAN BE CHANGED FOR IPHONE

# TODO CHANGE FOR KITTI
# points_ZoI = numpy.float32([(500, 360),  # top-right of
#                             (0, 200),
#                             (0, 370),  # bottom-left of ZoI
#                             (1226, 370),  # bottom-right of ZoI
#                             (1226, 200),
#                             (780, 240)])  # top-left of ZoI
# TODO CHANGE FOR IPHONE
# points_ZoI = numpy.float32([(500, 400),  # top-right of
#                             (0, 420),
#                             (0, 720),  # bottom-left of ZoI
#                             (1280, 720),  # bottom-right of ZoI
#                             (1280, 420),
#                             (780, 400)])  # top-left of ZoI
# # TODO CHANGE FOR UDACITY
points_ZoI = numpy.float32([(500, 400),  # top-right of
                            (0, 420),
                            (0, 720),  # bottom-left of ZoI
                            (1280, 720),  # bottom-right of ZoI
                            (1280, 420),
                            (780, 400)])  # top-left of ZoI



# _____DEPENDING ON USER INPUT OF LANE COUNT -> ROIs will be switched
points_birdeye_transform = {

    "Lane_1": {'source_points': numpy.float32([(580, 460),  # source-points top-right
                                               (205, 720),  # bottom-left
                                               (1110, 720),  # bottom-right,
                                               (703, 460)]),  # top-left
               'destination_points': numpy.float32([(420, 0),  # destination-points top-right,
                                                    (420, 720),  # bottom-left,
                                                    (750, 720),  # bottom-right,
                                                    (750, 0)])},  # top-left

    # TODO NIGHT FRAMES
    "Lane_2": {'source_points': numpy.float32([(460, 460),  # source-points top-right
                                               (200, 720),  # bottom-left
                                               (1050, 720),  # bottom-right,
                                               (600, 460)]),  # top-left
               'destination_points': numpy.float32([(420, 0),  # destination-points top-right,
                                                    (420, 720),  # bottom-left,
                                                    (750, 720),  # bottom-right,
                                                    (750, 0)])},  # top-left
    "Lane_3": {'source_points': numpy.float32([(580, 460),  # source-points top-right -> Image plane is top left
                                               (205, 720),  # bottom-left
                                               (1110, 720),  # bottom-right,
                                               (703, 460)]),  # top-left
               'destination_points': numpy.float32([(420, 0),  # destination-points top-right,
                                                    (420, 720),  # bottom-left,
                                                    (750, 720),  # bottom-right,
                                                    (750, 0)])},  # top-left
    #TODO IPHONE CASE
    "Lane_4": {'source_points': numpy.float32([(575, 345),  # source-points top-right
                                               (104, 570),  # bottom-left
                                               (1050, 570),  # bottom-right,
                                               (475, 345)]),  # top-left
               'destination_points': numpy.float32([(750, 0),  # destination-points top-right,
                                                    (420, 720),  # bottom-left,
                                                    (750, 720),  # bottom-right,
                                                    (420, 0)])},  # top-left
    # TODO KITTI
    "Lane_5": {'source_points': numpy.float32([(670, 150),  # source-points top-right
                                               (380, 360),  # bottom-left
                                               (820, 360),  # bottom-right,
                                               (530, 150)]),  # top-left
               'destination_points': numpy.float32([(500, 0),  # destination-points top-right,
                                                    (530, 370),  # bottom-left,
                                                    (750, 370),  # bottom-right,
                                                    (750, 0)])}  # top-left
}
param_curve_class = {'num_of_windows': 15,  # number of windows used for curve-sliding
                     'histogram_width': 75,  # width of histogram for a curve, by default 200
                     'histogram_seed': 65,  # histogram seed used for vertical slices of histogram
                     'histogram_vertical_ratio_end': 1,  # height ratio used for histogram, 0.6 by default to end in ~2/3 from the top
                     'histogram_vertical_ratio_start': 0,  # height ratio used for histogram, 0 by default to start from the top
                     'histogram_ratio_localmax': 1,  # ratio of second max comparing to 1st, to be get picked
                     'offset_cam': 0,  # offset from vehicle's centerline to camera's centerline (positive if cam on vehicle's left)
                     'm_vehicle_width': 1.8,  # [m] body width of ford focus 1999
                     'm_look_ahead': 10,  # [m] look ahead distance, used for curvature and space-left-in-lane estimation
                     'margin_x': 50,  # width of windows used for curve-sliding, by default 100
                     'min_pixel_inside': 50,  # min pixel number inside a window
                     'max_pixel_inside': 4500,  # max pixel number inside a window to be considered as a line
                     'max_width_not_a_line': 115,  # max width of a line, to eliminate much dispersed detected pixels
                     'min_pixel_confindex': 50,  # min pixel number inside a window, used for confindence evaluation
                     'xm_by_pixel': 3.7 / 700,  # meter by pixel in x direction (horizontal)
                     'ym_by_pixel': 30 / 720,  # meter by pixel in y direction (vertical)
                     'thd_confindex': 33,  # min confindence of a line
                     'min_pixel_bold': 150,  # min pixel inside a window of bold-type line (bold : barrier or road-edge)
                     'min_pixel_doubleline': 150,  # min pixel inside a window of double-line-type line
                     'doubleline_width_px': 50,  # min width of a double-line, in pixel unit
                     'bold_width_px': 75,
                     'merge_thresh'  : 0.5,  #pixels
                     'diverge_thresh': 2}    #pixels  # min width of a bold line, in pixel unit

param_curve_class = {'num_of_windows': 15,  # number of windows used for curve-sliding
                     'histogram_width': 75,  # width of histogram for a curve, by default 200
                     'histogram_seed': 65,  # histogram seed used for vertical slices of histogram
                     'histogram_vertical_ratio_end': 0.8,  # height ratio used for histogram, 0.6 by default to end in ~2/3 from the top
                     'histogram_vertical_ratio_start': 0,  # height ratio used for histogram, 0 by default to start from the top
                     'histogram_ratio_localmax': 1,  # ratio of second max comparing to 1st, to be get picked
                     'offset_cam': 0,  # offset from vehicle's centerline to camera's centerline (positive if cam on vehicle's left)
                     'm_vehicle_width': 1.8,  # [m] body width of ford focus 1999
                     'm_look_ahead': 15,  # [m] look ahead distance, used for curvature and space-left-in-lane estimation
                     'margin_x': 50,  # width of windows used for curve-sliding, by default 100
                     'min_pixel_inside': 50,  # min pixel number inside a window
                     'max_pixel_inside': 3000,  # max pixel number inside a window to be considered as a line
                     'max_width_not_a_line': 115,  # max width of a line, to eliminate much dispersed detected pixels
                     'min_pixel_confindex': 50,  # min pixel number inside a window, used for confindence evaluation
                     'xm_by_pixel': 3.7 / 700,  # meter by pixel in x direction (horizontal)
                     'ym_by_pixel': 30 / 720,  # meter by pixel in y direction (vertical)
                     'thd_confindex': 33,  # min confindex of a line
                     'min_pixel_bold': 150,  # min pixel inside a window of bold-type line (bold : barrier or road-edge)
                     'min_pixel_doubleline': 150,  # min pixel inside a window of double-line-type line
                     'doubleline_width_px': 50,  # min width of a double-line, in pixel unit
                     'bold_width_px' : 75,        # min width of a bold line, in pixel unit
                     'merge_thresh'  : 0.8,  #pixels
                     'diverge_thresh': 1.5}    #pixels