
import cv2
from scripts.lib import settings


class birdseye_perspective:
    def __init__(self, lane_count):

        self.shape = settings.UNWARPED_SIZE
        # ROI Points keys defined as "Lane_<LaneCount>"
        self.roiPoints          = settings.points_birdeye_transform["Lane_"+str(lane_count)]
        self.birdeye_matrix     = cv2.getPerspectiveTransform(self.roiPoints["source_points"], self.roiPoints["destination_points"])
        self.inv_birdeye_matrix = cv2.getPerspectiveTransform(self.roiPoints["destination_points"], self.roiPoints["source_points"])
        self.frame_skyView      = None
        self.frame_camView      = None

    def apply_skyview(self, frame_camview):
        """apply birdseye view transform
        @param frame_camview-> Full camera frame as an RBG matrix
        @return: self.frame_skyView-> Bird's eye perspective of current RGB Frame"""

        shape = (frame_camview.shape[1], frame_camview.shape[0])
        frame_skyView      = cv2.warpPerspective(frame_camview, self.birdeye_matrix, shape)  # , flags = cv2.INTER_LINEAR
        self.frame_skyView = frame_skyView
        self.frame_camView = frame_camview
        return self.frame_skyView

    def apply_vehicleview(self, frame_skyView):
        """apply reversed birdseye view transform to get back to camera view
         @param frame_skyView-> Bird's eye perspective as an RBG matrix
        @return: self.frame_camView-> Back transformed perspective of current frame to camera frame """

        shape              = (frame_skyView.shape[1], frame_skyView.shape[0])
        frame_camView      = cv2.warpPerspective(frame_skyView, self.inv_birdeye_matrix, shape)
        self.frame_camView = frame_camView
        self.frame_skyView = frame_skyView

        return self.frame_camView
