from hashlib import new
import cv2
from scripts.lib import settings
from scripts.lib import pipeline_lanefinding as lane_finding
from scripts.lib import pipeline_preprocessing as pre_processing
from scripts.lib.lib_photography import *
from scripts.lib import lib_perspective as perspective
from scripts.lib import lib_linedetection as linedetection
from scripts.lib.lib_frame import *
from copy import deepcopy
import time
import numpy as np


def main():
    '''Testing script for  perspective transform coupled with ED Line detection algo'''

    cap = cv2.VideoCapture("data/videos_test/IMG_2578.MOV")
    # cap = cv2.VideoCapture(0)
    out = cv2.VideoWriter('DocumentationResources/Videos/output_IMG_2578.MOV',
                          cv2.VideoWriter_fourcc(
                              *'mp4v'), settings.dst_frameRate,
                          (settings.ORIGINAL_SIZE[1], settings.ORIGINAL_SIZE[0]))

    preprocessing_obj1 = pre_processing.pipeline_preprocessing(
        settings.param_1)
    preprocessing_obj2 = pre_processing.pipeline_preprocessing(
        settings.param_2)
    preprocessing_obj = pre_processing.pipeline_preprocessing(
        settings.param_2)
    perspective_obj = perspective.birdseye_perspective(2)
    perspective_obj1 = perspective.birdseye_perspective(2)
    perspective_obj2 = perspective.birdseye_perspective(2)

    lanedetection_obj = lane_finding.pipeline_lanefinding()
    count = 0
    frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    while cap.isOpened():
        start_time = time.time()  # start timing
        # This line can be changed with read frames from another ros node
        ret, frame = cap.read()
        frame = rescale_frame(frame, 66.67)

        # Apply sobel processing on normal view and get the birds eye view from it.
        preprocessing_obj1.apply_pipeline(frame)
        perspective_obj1.apply_skyview(
            frameHLS2RGB(preprocessing_obj1.frame_HLS))
        # cv2.imshow('framehls sky', perspective_obj.frame_skyView)

        # Apply Sobel processing on BEV directly
        perspective_obj.apply_skyview(frame)
        preprocessing_obj.apply_pipeline(perspective_obj.frame_skyView)

        # Fuse detected edge masks from image-> preprocessing + Sobel -> BEV  and image -> BEV -> preprocessing + Sobel
        fused = deepcopy(perspective_obj1.frame_skyView) | deepcopy(frameHLS2RGB(
            preprocessing_obj.frame_HLS))

        # kernel = np.ones((5, 5), np.uint8)
        # fused = cv2.dilate(fused, kernel, iterations=1)

        # Image Sharpening
        kernel = np.array([[0, -1, 0],
                           [-1, 5, -1],
                           [0, -1, 0]])
        fused = cv2.filter2D(src=fused, ddepth=-1, kernel=kernel)

        # Second time preprocessing + ED lines on fused BEV
        preprocessing_obj2.apply_pipeline_noSobel(
            fused, apply_mask=True)

        # ED lines directly on image
        linedrawing_obj = linedetection.line_detection(deepcopy(cv2.cvtColor(
            frame, cv2.COLOR_RGB2GRAY)))  # only using 1 time processing rn

        linedrawing_obj.ED_Lines()
        mask = np.zeros((frame.shape[0], frame.shape[1]), dtype=np.uint8)
        frame_copy = deepcopy(frame)

        for l in linedrawing_obj.lines:
            line_start, line_end = tuple(l[:2]), tuple(l[2:4])

            m = abs(np.rad2deg(np.arctan2(
                (line_end[1] - line_start[1]), (line_end[0] - line_start[0]))))
            if 10 < m <= 170:
                cv2.line(mask, line_start, line_end, (255, 255, 255), 1)

        for i in range(mask.shape[0] - 1):
            for j in range(mask.shape[1] - 1):
                if mask[i][j] == 0:
                    frame_copy[i][j] = [0, 0, 0]

        # Convert ED line detected frame to BEV
        perspective_obj2.apply_skyview(frame_copy)

        # Fuse (fused image -> 2nd preprocess + ED lines) and (image -> ED lines -> BEV)
        final_fused = deepcopy(perspective_obj2.frame_skyView) | deepcopy(frameHLS2RGB(
            preprocessing_obj2.frame_HLS))

        preprocessing_obj2.frame_HLS = final_fused

        lanedetection_obj.apply_pipeline(
            frame, preprocessing_obj1, preprocessing_obj2, perspective_obj1, perspective_obj2)

        cv2.imshow("LinesDrawn", lanedetection_obj.dst_frame)
        out.write(lanedetection_obj.dst_frame)
        count = count+1
        print(count, "out of", frame_count, "frames")
        elapsed_time = time.time() - start_time  # end timing
        print('Frame Preprocess Complete: Time: {0:.3f}s'.format(
            elapsed_time))

        if cv2.waitKey(10) & 0xFF == ord('q'):
            break

    # mean preprocess time for each image
    cap.release()
    out.release()

    # cv2.destroyWindow("RGB FRAMES")  # destroy all opened windows
    cv2.destroyAllWindows()

    return 0


if __name__ == '__main__':
    main()
