import cv2
from scripts.lib import settings
from scripts.lib import pipeline_preprocessing as pre_processing
import time

'''https://answers.opencv.org/question/120277/default-color-space-for-frames-in-video/'''

def main():
    '''Testing script for video pre processing and perspective transform'''

    cap = cv2.VideoCapture("data/videos_test/test_video_1.mp4")
    # cap = cv2.VideoCapture(0)

    count = 0
    while cap.isOpened():
        start_time = time.time()  # start timing
        #This line can be changed with read frames from another ros node
        ret, frame = cap.read()

        cv2.imshow('RGB FRAMES', frame)
        preprocessing_obj = pre_processing.pipeline_preprocessing(settings.param_1)
        preprocessing_obj.apply_pipeline(frame)
        cv2.imshow("Processed Image", preprocessing_obj.frame_HLS)
        elapsed_time = time.time() - start_time  # end timing
        print('Frame Preprocess Complete: Time: {0:.1f}s'.format(elapsed_time))
        if cv2.waitKey(10) & 0xFF == ord('q'):
            break

    cap.release()
    # cv2.destroyWindow("RGB FRAMES")  # destroy all opened windows
    cv2.destroyAllWindows()

    return 0


if __name__ == '__main__':
    main()
