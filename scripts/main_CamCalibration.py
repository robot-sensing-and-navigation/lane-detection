import cv2
import glob
import natsort
import json
import numpy
from scripts.lib import pipeline_camcalibration as calib

""" documentation & tutorials
https://docs.opencv.org/master/dc/dbb/tutorial_py_calibration.html
https://docs.opencv.org/2.4/modules/calib3d/doc/camera_calibration_and_3d_reconstruction.html
"""

"""
Load chessboards photos and find camera matrix and distorsion coefficient
Save camera calibration data into S7_cam_calibration.p for further usage
"""

#DONE ABHINAV
def generate_calibrationFile(imagePaths):
    '''Caller function for camera calibration file generation
    @param imagePaths: Paths to checkerboard pattern
    @return: None
    '''

    termCriteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
    patternShape = [9, 6]
    camCalib_obj = calib.pipeline_camcalibration(imagePaths)
    camCalib_obj.gen_CalibParams(termCriteria, patternShape)


def test_correction(framesList_BGR):
    '''Unit testing of distortion correction
    @param framesList_BGR: List of paths to uncorrected BGR frames
    @return: None
    '''

    for framePath_BGR in framesList_BGR:
        distorted_frame = cv2.imread(framePath_BGR)
        cv2.imshow("BGR Image", distorted_frame)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
        # TODO
        calibParam_path = "data/camCalibration/calibrationConfig_/CalibrationConfig.json"
        with open("data/camCalibration/calibrationConfig_/CalibrationConfig.json", 'r', encoding="utf8") as infile:
            calibration_ = json.load(infile)
        print("Parameters loaded")

        camCorrection_obj = calib.pipeline_camcalibration(None)
        corrected_frame   = camCorrection_obj.apply_undistort(distorted_frame, numpy.array(calibration_["camera_matrix"]), numpy.array(calibration_["distortion_coefficient"]))
        cv2.imshow("Corrected BGR Frame", corrected_frame)
        cv2.waitKey(200)
        cv2.destroyAllWindows()


def main():
    # Path is defined as a resource root, thus .. indexing is not considered here
    camCalibImage_List = natsort.natsorted(glob.glob('data/camCalibration/input_chessboard/*.jpg'), reverse=False)
    generate_calibrationFile(camCalibImage_List)

    test_FramesBGR = natsort.natsorted(glob.glob('data/images_test/*.jpg'), reverse=False)
    test_correction(test_FramesBGR)

    return 0


if __name__ == '__main__':
    main()
