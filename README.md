# LaneDetection_EECE5554_FinalProject
Team code for the final project demo in the Robotic sensing and Navigation class at Northeastern University 

Clone using SSH ZIP download option to utilize ssh key on your system. 

## Info on General python paths (IDE independent)
[This link provides info on how to set the source root, data root etc](https://code.visualstudio.com/docs/python/environments#_environment-variable-definitions-file)
## PyCharm setup instructions
* Using a pipenv environment to setup all modules using `pipenv install`
* `pipenv shell` opens the pipenv console for virtual environment specific coding
* The debugger on PyCharm can be configured to use this p
* ipenv
  * After installing all modules using `pipenv install`, copy the output of the following command in 
  console of the project working directory: `pipenv --venv`
  * Open **_Project Settings_**, and then select project interpreter
  * _**Add a Python Interpreter > System Interpreter > Select Python Interpreter**_ and
  * paste the output from the first command, appending **_/bin/python_** onto the end.
  * To debug using this pipenv
    * Go to **run configuration settings** of the file to debug using a right click in the editor 
    area **_More Run/Debug > Modify Run Configuration_**
    * Paste the intended working path in the working directory field
    

## Colorspace Information
* OpenCV does not keep track of colorspace by default
* All frames converted to [BGR space](https://answers.opencv.org/question/120277/default-color-space-for-frames-in-video/)
* Every read frame will thus be pushed into the pipeline after a _**BGR to RGB reordering**_

## Image Preprocessing Pipeline

* Exposure balance : to correct the average exposure value of a frame
* White balance : to correct the white balance exposure value of a frame
* Highlight removing : to remove highlights, inspired by Lightroom
* Shadow removing : to remove shadows, inspired by Lightroom With these photography's post-processing inspired technics, 
we could archieve good results in various light condition and filter some noises

Sobel Filter slows the preprocessing pipeline considerably, the frequency dropping to roughly 10FPS. The following image shows the preprocessed image
![Sobel filter preprocessed frame](DocumentationResources/Images/PreProcessingPipeline_SLOW.png)

Points to ponder about:
* Should we preprocess with sobel filter then try edge detection or skip sobel filtering for other alternatives
* Should we preprocess only the ROI?
  * What should the ROI look like in this case? (maybe half the image)
* Is ED lines a viable alternative to sobel filtering

    
## ROI generation, Perspective Transformation & Line detection
The following image shows what the intended ROI for lane detection is considered
![Sample ROI](DocumentationResources/Images/SampleROI.png)

The ROI for line and eventually lane detection is generated based on dynamic user inputs of
the lane count and a pre-requisite knowledge of the typical lane-width in the US.
A perspective transformation matrix maps a trapezoidal ROI on the bottom half
of the RGB frame to a bird's eye perspective to help straighten out curved lane markings.

 Since ED Lines is using just grayscale imagery, preprocessing is skipped for this test.
 The following image shows the output of the perspective transformation. The detected edges are also overlayed (White-> Canny; Red-> ED Line).
![Line Detection tests Canny vs ED Lines in sky view perspective](DocumentationResources/Images/LineDetection_PerspectiveTForm.png)

The _lib_linedetection.py_ file is used to call either the ED Line drawing library or the Canny Edge detector.
_main_LineDetection.py_ tests both algorithms and writes the output to a video file.


## ROS Nodes

* Master
  * Start a terminal
  * source setup.bash
  * roscore

* Publishers
  * (node_readVideo.py)
    * Publishes to /CameraData topic with message type std_msgs.Image
    * How to run
      * Start another terminal
      * source setup.bash
      * nodes/node_readVideo.py data/iphone_vid/Highway.MOV 1 0.1 100

* Subscribers 
  * (node_computeRawPoints.py)
    * Subscribes to std::msgs Image type topic named /CameraData published by node_readVideo.py and writes to a video on a per frame (post processed) basis
    * How to run
      * Start another terminal
      * source setup.bash
    * nodes/node_readVideo.py
  * (node_overlaySubscriber.py)
    * Subscribes to _____
    * How to run
      * Start another terminal
      * source setup.bash
      * nodes/node_readVideo.py data/iphone_vid/Highway.MOV 1 0.1 100


## GENERAL INFORMATION
[Wrapping C++ Libs in Python](https://betterprogramming.pub/how-to-create-a-python-wrapper-for-c-c-shared-libraries-35ffefcfc10b
)

